﻿using System.Net;
using System.Net.Sockets;
using System.Text;

string ingressIpAddressStr = "34.175.62.58";
int ingressPort = 80;
int bytesSent = 0;

IPAddress ingressIpAddress = IPAddress.Parse(ingressIpAddressStr);
IPEndPoint hostep = new(ingressIpAddress, ingressPort);
Socket socket = new(AddressFamily.InterNetwork, SocketType.Stream, ProtocolType.Tcp);

socket.Connect(hostep);

string requestUrl = "http://members.dyndns.org/nic/update?system=dyndns&hostname=carlosjulio.rfidcreator.com&myip=&wildcard=OFF&mx=NO&backmx=NO&offline=NO";

string request = $"GET {requestUrl} HTTP/1.1\r\n" +
    "Host: members.dyndns.org\r\n" +
    "Authorization: Basic YWRtaW46YWRtaW4=\r\n" +
    "User-Agent: Hikvision-dvrdvs-1.0.0\r\n" +
    "\r\n";

bytesSent = socket.Send(Encoding.UTF8.GetBytes(request));

byte[] buffer = new byte[1024];

int numberOfBytesReceived = socket.Receive(buffer, buffer.Length, 0);
string page = Encoding.ASCII.GetString(buffer, 0, numberOfBytesReceived);
Console.WriteLine(page);
socket.Close();